package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        System.out.println("Let's play round " + roundCounter);

        while(true) {
			
            //Get the user's move through user input
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String myMove = sc.nextLine();
            
            
            //Check if the user's move is valid (rock, paper, or scissors)
            if(!myMove.equals("rock") && !myMove.equals("paper") && !myMove.equals("scissors")) {
                
                System.out.println("I do not understand " + myMove + ". Could you try again?");
                
            }
            else {
                
                //Get a random number in between 0 and 3 and convert it to an integer so that the possibilities are 0, 1, or 2
                int rand = (int)(Math.random()*3);
                
                //Convert the random number to a string using conditionals and print the opponent's move
                String opponentMove = "";
                if(rand == 0) {
                    opponentMove = "rock";
                } else if(rand == 1) {
                    opponentMove = "paper";
                } else {
                    opponentMove = "scissors";
                }
                
                //Print the results of the game
                roundCounter+=1;
                if(myMove.equals(opponentMove)) {
                    System.out.println("Human chose " + myMove + ", computer chose "+ opponentMove + ". It's a tie!");
                } else if((myMove.equals("rock") && opponentMove.equals("scissors")) || (myMove.equals("scissors") && opponentMove.equals("paper")) || (myMove.equals("paper") && opponentMove.equals("rock"))) {
                    System.out.println("Human chose "+ myMove + ", computer chose " + opponentMove + ". Human wins!");
                    //Increment humanScore
                    humanScore++;
                } else {
                    System.out.println("Human chose "+ myMove + ", computer chose " + opponentMove + ". Computer wins!");
                    //Increment computerScore
                    computerScore++;
                }
                
                //Print out score
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                
                //Check if the user wants to quit the game
                System.out.println("Do you wish to continue playing? (y/n)?");
                myMove = sc.nextLine();

                if(myMove.equals("n")) {
                    break;
                }
                if(myMove.equals("y")) {
                    System.out.println("Let's play round " + roundCounter);

                    continue;
                }
            }
                
            }
        //Print a final message for the user
		System.out.println("Bye bye :)");






        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
